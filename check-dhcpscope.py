#!/usr/bin/env python

# Import modules
import argparse
from sys import exit
from subprocess import check_output

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-s", "--server", required=True,  help="Netbios name of the DHCP-server")

args = vars(ap.parse_args())

server = args["server"]

# Function to return message and exit code to Nagios
def stattonagios(msg, status):
  if status == 0:
    statmsg = "OK: " + msg
  elif status == 1:
    statmsg = "WARNING: " + msg
  elif status == 2:
    statmsg = "CRITICAL: " + msg
  elif status == 3:
    statmsg = "UNKNOWN: " + msg

  print(statmsg)
  exit(status)

# Check status of scopes on DHCP server
scopes=str(check_output("netsh dhcp server \\\\" + server + " show scope", shell=True))

if scopes.find("Disabled") == -1:
	msg = "All scopes are active."
	status = 0
	stattonagios(msg, status)
else:
	msg = "There are inactive scopes."
	status = 1
	stattonagios(msg, status)